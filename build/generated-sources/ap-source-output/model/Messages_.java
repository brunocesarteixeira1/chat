package model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-06-24T11:29:21")
@StaticMetamodel(Messages.class)
public class Messages_ { 

    public static volatile SingularAttribute<Messages, Integer> id;
    public static volatile SingularAttribute<Messages, Date> sentAt;
    public static volatile SingularAttribute<Messages, String> message;
    public static volatile SingularAttribute<Messages, Integer> user;
    public static volatile SingularAttribute<Messages, Integer> room;

}