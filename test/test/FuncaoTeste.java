/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.util.ArrayList;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import model.Messages;
import java.util.List;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Root;
import service.MessagesFacadeREST;
import service.SearchCriteria;

/**
 *
 * @author billy
 */
public class FuncaoTeste {

    public static void main(String args[]) {
        testList();
    }
    
    public static void testeConsulta() {
            
        EntityManager em = getEntityManager();

//        try {
//            em.getTransaction().begin();
//            Funcao funcao = new Funcao();
//            funcao.setFunNome("Epicolo");
//            em.persist(funcao);
//            em.getTransaction().commit();
//
//        } catch (Exception ex) {
//            em.getTransaction().rollback();
//            System.out.println("Erro: " + ex);
//        } finally {
//            em.close();
//        }

        /*
         * CriteriaBuilder builder = session.getCriteriaBuilder();
         * CriteriaQuery<Department> query = builder.createQuery(Department.class);
         * Root<Department> root = query.from(Department.class);
         * query.select(root).where(builder.equal(root.get("id"), 1l));
         * Query<Department> q=session.createQuery(query);
         * Department department=q.getSingleResult();
         * System.out.println(department.getName());
         */
        
        try {
            //  em == session
            //  cb == builder
            //  cq == query
            CriteriaBuilder cb = em.getCriteriaBuilder();
            //CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
            CriteriaQuery<Messages> cq = cb.createQuery(Messages.class);
            Root<Messages> root = cq.from(Messages.class);
            
            cq.select(root).where(cb.like(root.get("message"), "%Hi%"));
            Query q = em.createQuery(cq);
            
            System.out.println("q = " + q);
            
            List<Messages> messages = q.getResultList();
            for (Messages message : messages) {
                System.out.println(message.getMessage());
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            em.close();
        }
    }
    
    public static void testList() {
        
//        MessagesFacadeREST mfr = new MessagesFacadeREST();
//        
//        //SearchCriteria sc1 = new SearchCriteria("message","like","%nal%");
//        SearchCriteria sc2 = new SearchCriteria("room","<","6");
//        //SearchCriteria sc3 = new SearchCriteria("user",">","1");
//        
//        List<SearchCriteria> params = new ArrayList<>();
//        
//        //params.add(sc1);
//        params.add(sc2);
//        //params.add(sc3);
//        
//        List<Messages> messages = mfr.list(params);
//        for (Messages message : messages) {
//            System.out.println(message.getMessage());
//        }
                
    }

    protected static EntityManager getEntityManager() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("CHATPU");
        EntityManager em = factory.createEntityManager();
        return em;
    }

}
