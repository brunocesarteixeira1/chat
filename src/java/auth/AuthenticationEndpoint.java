package auth;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.HashMap;
import javax.ws.rs.Path;
import javax.ws.rs.POST;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import org.apache.commons.codec.digest.HmacUtils;
import org.json.JSONObject;

@Path("/authentication")
public class AuthenticationEndpoint {

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String authenticateUser(String body) {
        try {
            int session = 1200; // session 20 minutos
            
            String senha = "minha-senha";
            String usuario = "meu-usuario";
            
            System.out.println(System.currentTimeMillis() / 1000);
            int a = (int) (System.currentTimeMillis() / 1000);
            a += session;
            System.out.println(a);
            
            String mil = String.valueOf(System.currentTimeMillis() / 1000);

            System.out.println("senha: " + senha
                    + ", usuario: " + usuario
                    + ", mil: " + mil
            );

            String key = "";
            String header;
            String payload;
            String signature;

            //#################################
            HashMap<String, String> hashHeader = new HashMap<String, String>();
            hashHeader.put("typ", "JWT");
            hashHeader.put("alg", "HS256");
            System.out.println("hashHeader = " + hashHeader);

            JSONObject json = new JSONObject(hashHeader);
            System.out.println("hashHeader json = " + json);

            header = this.getBase64Parsed(json.toString());
            System.out.println("hashHeader json Base64 = " + header);

            //#################################
            HashMap<String, String> hashPayload = new HashMap<String, String>();
            hashPayload.put("iss", "sct.infogruposi.com");
            hashPayload.put("username", "brunokchimbo");
            hashPayload.put("email", "brunokchimbo@hotmail.com");
            hashPayload.put("acl", "Administrador");
            hashPayload.put("exp", String.valueOf(a));
            System.out.println("hashPayload = " + hashPayload);

            json = new JSONObject(hashPayload);
            System.out.println("hashPayload json = " + json);

            payload = this.getBase64Parsed(json.toString());
            System.out.println("hashPayload json Base64 = " + payload);

            //#################################
            String token = header + "." + payload;

            //#################################
            //geracao da key
            //#################################
            /*
            senha = md5(senha);
            usuario = md5(usuario);
            mil = md5(mil);
            
            int s = senha.length();
            int u = usuario.length();
            int m = mil.length();
            
            int x;
            if (s <= u && s <= m) {
                x = s;
            } else if (u <= m && u <= s) {
                x = u;
            } else {
                x = m;
            }
            
            for (int y = 0; y < x; y++) {
                key = key + senha.charAt(y);
                key = key + usuario.charAt(y);
                key = key + mil.charAt(y);
                System.out.print(senha.charAt(y));
                System.out.print(usuario.charAt(y));
                System.out.print(mil.charAt(y));
            }
             */
            String[] palavras = {senha, usuario, mil};
            key = geraKey(palavras);
            System.out.println("key: " + key);
            //#################################
            //fim geracao da key
            //#################################

            String chave = this.getHmacSHA256Encrypted(token, key);
            System.out.println("signature hashHmacSHA256 = " + chave);

            signature = this.getBase64Parsed(chave);
            System.out.println("signature hashHmacSHA256 Base64 = " + signature);

            //#################################
            token = token + "." + signature;
            System.out.println("token completo = " + token);

            return "Seu token: " + token;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "Barrado";
    }

    private void authenticate(String username, String password) throws Exception {
        // Authenticate against a database, LDAP, file or whatever
        // Throw an Exception if the credentials are invalid
    }

    private String issueToken(String username) {
        // Issue a token (can be a random String persisted to a database or a JWT token)
        // The issued token must be associated to a user
        // Return the issued token
        return null;
    }

    public String getHmacSHA256Encrypted(String value, String key) {
        return HmacUtils.hmacSha1Hex(key, value);
        /*
            
            // Get an hmac_sha1 key from the raw key bytes
            byte[] keyBytes = key.getBytes();
            SecretKeySpec signingKey = new SecretKeySpec(keyBytes, "HmacSHA1");
            
            // Get an hmac_sha1 Mac instance and initialize with the signing key
            Mac mac = Mac.getInstance("HmacSHA1");
            mac.init(signingKey);
            
            // Compute the hmac on input data bytes
            byte[] rawHmac = mac.doFinal(value.getBytes());
            
            // Convert raw bytes to Hex
            byte[] hexBytes = new Hex().encode(rawHmac);
            
            //  Covert array of Hex bytes to a String
            return new String(hexBytes, "UTF-8");

         */

    }

    public String getBase64Parsed(String msg) {
        return Base64.getEncoder().withoutPadding().encodeToString(msg.getBytes());
    }

    public String md5(String s) throws NoSuchAlgorithmException {
        MessageDigest m = MessageDigest.getInstance("MD5");
        m.update(s.getBytes(), 0, s.length());
        String md5ado = new BigInteger(1, m.digest()).toString(16);
        return md5ado;
    }

    public String geraKey(String args[]) throws NoSuchAlgorithmException {
        for (int x = 0; x < args.length; x++) {
            args[x] = md5(args[x]);
            System.out.println("args[" + x + "] md5: " + args[x]);
        }
        
        bubbleSort(args);

        String key = "";
        for (int y = 0; y < args[0].length(); y++) {
            for (int z = 0; z < args.length; z++) {
                key = key + args[z].charAt(y);
                System.out.print(args[z].charAt(y));
            }
        }

        System.out.println("\n");
        
        return key;
    }

    public void bubbleSort(String[] arr) {
        int n = arr.length;
        String temp;
        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {
                if (arr[j - 1].length() > arr[j].length()) {
                    temp = arr[j - 1];
                    arr[j - 1] = arr[j];
                    arr[j] = temp;
                }

            }
        }

    }
}