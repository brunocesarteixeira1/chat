package auth;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

/**
 *
 * @author bruno.teixeira
 */
public class GeradorPalavra {

    public static void main(String args[]) throws NoSuchAlgorithmException {
        /*
        String s = "senha";
        MessageDigest m = MessageDigest.getInstance("MD5");
        m.update(s.getBytes(),0,s.length());
        System.out.println("MD5: "+new BigInteger(1,m.digest()).toString(16));
        
        s = "usuario";
        m = MessageDigest.getInstance("MD5");
        m.update(s.getBytes(),0,s.length());
        System.out.println("MD5: "+new BigInteger(1,m.digest()).toString(16));
        
        s = "1539655200";
        m = MessageDigest.getInstance("MD5");
        m.update(s.getBytes(),0,s.length());
        System.out.println("MD5: "+new BigInteger(1,m.digest()).toString(16));
        
        s = "parangaricotirimirruaro";
        m = MessageDigest.getInstance("MD5");
        m.update(s.getBytes(),0,s.length());
        System.out.println("MD5: "+new BigInteger(1,m.digest()).toString(16));
        
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        System.out.println("raba mil: " + System.currentTimeMillis()/1000);
        String mil = String.valueOf(System.currentTimeMillis()/1000);
        System.out.println("mil: " + mil);
        
        String palavras[]
                = {"banana",
                    "leva",
                    "chaves",
                    "certeza",
                    "pera",
                    "paralelepipedo",
                    "uva",
                    "marmelada"
                };

        bubbleSort(palavras);

        int i = 0;
        for (String temp : palavras) {
            System.out.println("palavras " + ++i + " : " + temp);
        }
        */
        
        System.out.println(System.currentTimeMillis() / 1000);
        int a = (int) (System.currentTimeMillis() / 1000);
        a += 1200;
        System.out.println(a);
    }

    public static void bubbleSort(String[] arr) {
        int n = arr.length;
        String temp;
        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {
                if (arr[j - 1].length() > arr[j].length()) {
                    temp = arr[j - 1];
                    arr[j - 1] = arr[j];
                    arr[j] = temp;
                }

            }
        }

    }
}
