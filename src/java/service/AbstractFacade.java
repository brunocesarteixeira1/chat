/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author billy
 */
public abstract class AbstractFacade<T> {

    private Class<T> entityClass;

    public AbstractFacade(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    protected EntityManager getEntityManager() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("CHATPU");
        EntityManager em = factory.createEntityManager();
        return em;
    }
    
    public T create(T entity) {
        EntityManager em = getEntityManager();
        try {
            em.getTransaction().begin();
            em.persist(entity);
            em.flush();
            em.getTransaction().commit();
            return entity;
        } catch (Exception e) {
            System.out.println("Erro: " + e);
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return null;
    }

    public void edit(T entity) {
        EntityManager em = getEntityManager();
        try {
            em.getTransaction().begin();
            em.merge(entity);
            em.getTransaction().commit();
        } catch (Exception e) {
            System.out.println("Erro: " + e);
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    public void remove(T entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public T find(Object id) {
        return getEntityManager().find(entityClass, id);
    }

    public List<T> findAll(String search) {

        try {
            System.out.println("search = " + search);

            EntityManagerFactory factory = Persistence.createEntityManagerFactory("CHATPU");
            EntityManager em = factory.createEntityManager();

            if (search != null) {

                List<SearchCriteria> params = new ArrayList<>();

                Pattern p = Pattern.compile("(\\w+?)(:|<|>)(\\w+?),");
                Matcher matcher = p.matcher(search + ",");
                while (matcher.find()) {
                    params.add(new SearchCriteria(
                            matcher.group(1),
                            matcher.group(2),
                            matcher.group(3)));
                }

                CriteriaBuilder builder = em.getCriteriaBuilder();
                CriteriaQuery query = builder.createQuery();
                Root r = query.from(entityClass);

                Predicate predicate = builder.conjunction();

                for (SearchCriteria param : params) {
                    if (param.getOperation().equalsIgnoreCase(">")) {
                        predicate = builder.and(predicate,
                                builder.greaterThanOrEqualTo(r.get(param.getKey()),
                                        param.getValue().toString()));
                    } else if (param.getOperation().equalsIgnoreCase("<")) {
                        predicate = builder.and(predicate,
                                builder.lessThanOrEqualTo(r.get(param.getKey()),
                                        param.getValue().toString()));
                    } else if (param.getOperation().equalsIgnoreCase(":")) {
                        if (r.get(param.getKey()).getJavaType() == String.class) {
                            predicate = builder.and(predicate,
                                    builder.like(r.get(param.getKey()),
                                            "%" + param.getValue() + "%"));
                        } else {
                            predicate = builder.and(predicate,
                                    builder.equal(r.get(param.getKey()), param.getValue()));
                        }
                    }
                }
                query.where(predicate);

                return em.createQuery(query).getResultList();
            } else {
                CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
                cq.select(cq.from(entityClass));
                return getEntityManager().createQuery(cq).getResultList();
            }
        } catch (IllegalArgumentException ex) {
            System.out.println("Erro: " + ex);
        }
        
        return null;

    }

    public List<T> findRange(int[] range) {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<T> rt = cq.from(entityClass);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
    
}
